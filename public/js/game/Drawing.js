/**
 * Creates a Drawing object.
 * @param {CanvasRenderingContext2D} context The context this object will
 *   draw to.
 * @constructor
 */
function Drawing(context, player_context) {
  this.context = context;
  this.player_context = player_context;
}

/**
 * This is a factory method for creating a Drawing object.
 * @param {CanvasRenderingContext2D} context The context this object will
 *   draw to.
 * @return {Drawing}
 */
Drawing.create = function(context, player_context) {
  return new Drawing(context, player_context);
};

/**
 * This method creates and returns an Image object.
 * @param {string} src The path to the image
 * @param {number} width The width of the image in pixels
 * @param {number} height The height of the image in pixels
 * @return {Image}
 */
Drawing.createImage = function(src, width, height) {
  var image = new Image(width, height);
  image.src = src;
  return image;
};

/**
 * Clears the canvas context.
 */
Drawing.prototype.clear = function() {
  var canvas = this.context.canvas;
  this.context.clearRect(0, 0, canvas.width, canvas.height);
  var player_canvas = this.player_context.canvas;
  this.player_context.clearRect(0, 0, player_canvas.width, player_canvas.height);
};


Drawing.prototype.drawBackground = function(image_obj) {
  this.context.save();
  this.context.beginPath();
  
  this.context.drawImage(image_obj,0, 0, 750, 700);
  
  this.context.restore();
};

Drawing.prototype.showPlayerLife = function(player) {
  this.player_context.save();
  this.player_context.beginPath();
  
  this.player_context.font = "10vh Arial";
  this.player_context.color = "yellow";
  this.player_context.fillText(player.lifeNo + "%", 50, 100);
  
  this.player_context.restore();
}


Drawing.prototype.showSelf = function(image_obj, selfPlayer, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  // this.context.drawImage(image_obj, 16 * 1, 16*4, 16, 16, selfPlayer.x * tileW, selfPlayer.y * tileH, tileW, tileH);
  // this.context.drawImage(image_obj, 16 * selfPlayer.imageW[parseInt(selfPlayer.frameId)], 
  // 16 * selfPlayer.imageH[parseInt(selfPlayer.frameId)], 16, 16, 
  // selfPlayer.x * tileW, selfPlayer.y * tileH, tileW, tileH);
  if (selfPlayer.avatar == "gremix") {
    this.context.drawImage(image_obj, 0, 0, 30, 45, selfPlayer.x * tileW, selfPlayer.y * tileH, tileW, tileH);
  }
  if (selfPlayer.avatar == "monkey") {
    this.context.drawImage(image_obj, 0, 0, 40, 45, selfPlayer.x * tileW, selfPlayer.y * tileH, tileW, tileH);  
  }
  if (selfPlayer.avatar == "girl") {
    this.context.drawImage(image_obj, 0, 0, 70, 85, selfPlayer.x * tileW, selfPlayer.y * tileH, tileW, tileH);  
  }
  this.context.restore();
};


Drawing.prototype.showOther = function(image_obj, otherPlayer, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  this.context.drawImage(image_obj, 0, 0, 60, 80, otherPlayer.x * tileW, otherPlayer.y * tileH, tileW, tileH);
  this.context.restore();
};


Drawing.prototype.showBoxes = function(image_obj, boxes, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  for (box of boxes) {
    this.context.drawImage(image_obj, 0, 0, 50, 55, box.x * tileW, box.y * tileH, tileW, tileH);
  }
  this.context.restore();
};


Drawing.prototype.showWalls = function(image_obj, image_obj1, walls, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  for (wall of walls) {
    if (wall.x == 1 ||  wall.y == 1 || wall.x == 13 || wall.y == 11) {
      this.context.drawImage(image_obj, 0, 0, 55, 55, wall.x * tileW, wall.y * tileH, tileW, tileH);    
    }
    else {
      // this.context.drawImage(image_obj, 16 * 10, 16 * 5, 16, 16, wall.x * tileW, wall.y * tileH, tileW, tileH);
      this.context.drawImage(image_obj1, 0, 0, 55, 55, wall.x * tileW, wall.y * tileH, tileW, tileH);
    }
  }
  this.context.restore();
};



Drawing.prototype.showEmpties = function(image_obj, empties, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  for (empty of empties) {
    this.context.drawImage(image_obj, 16, 0, 16, 16, empty.x * tileW, empty.y * tileH, tileW, tileH);
  }
  this.context.restore();
};


Drawing.prototype.showBombs = function(image_obj, bombs, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  for (bomb of bombs) {
    if (bomb.active == false) continue;
    this.context.drawImage(image_obj, 16 * bomb.imageW[parseInt(bomb.frameId)], 16 * bomb.imageH[parseInt(bomb.frameId)], 16, 16, bomb.x * tileW, bomb.y * tileH, tileW, tileH);
  }
  this.context.restore();
};


Drawing.prototype.showFires = function(image_obj, fires, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  for (fire of fires) {
    if (fire.active == false) continue;
    this.context.drawImage(image_obj, 16 * fire.imageW[parseInt(fire.frameId)], 16 * fire.imageH[parseInt(fire.frameId)], 16, 16, fire.x * tileW, fire.y * tileH, tileW, tileH);
  }
  this.context.restore();
};


Drawing.prototype.showBoost = function(image_obj, boost, tileW, tileH) {
  this.context.save();
  this.context.beginPath();
  if (boost.type == "bombBoost") {
    this.context.drawImage(image_obj, -6, -1, 35, 35, boost.x * tileW, boost.y * tileH, tileW, tileH);
  }
  if (boost.type == "healthBoost") {
    this.context.drawImage(image_obj, 0, 0, 500, 500, boost.x * tileW, boost.y * tileH, tileW, tileH);  
  }
  if (boost.type == "bombPutabilityBoost") {
    this.context.drawImage(image_obj, 0, 0, 250, 250, boost.x * tileW, boost.y * tileH, tileW, tileH);  
  }
  this.context.restore();
};

// Drawing.prototype.showMap = function(gameMap, tileImages) {
//   var tileW = 40, tileH = 40;
//   var mapW = 10, mapH = 10;
        
//   this.context.save();
//   this.context.beginPath();
//   for (var y = 0; y < mapH; y ++ ) {
//       for (var x = 0; x < mapW; x ++) {
//           switch (gameMap[x][y]) {
//               case 0: this.context.drawImage(tileImages, 16, 0, 16, 16, x * tileW, y * tileH, tileW, tileH);
//               break;
//               case 1: this.context.drawImage(tileImages, 16*10, 16*5, 16, 16, x * tileW, y * tileH, tileW, tileH);
//               break;
//               case 2: this.context.drawImage(tileImages, 16*9, 0, 16, 16, x * tileW, y * tileH, tileW, tileH);
//               break;
//               case 3: this.context.drawImage(tileImages, 16*1, 16*4, 16, 16, x * tileW, y * tileH, tileW, tileH);
//               break;
//           }
//       }
//   }
//   this.context.restore();
// }
