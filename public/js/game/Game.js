/**
 * Creates a Game on the client side to render the players and entities.
 * @constructor
 * @param {Object} socket The socket connected to the server.
 * @param {Drawing} drawing The Drawing object that will render the game.
 */
function Game(socket, drawing) {
  console.log('create game, ', socket.id);
  this.socket = socket;
  this.drawing = drawing;

  this.selfPlayer = null;
  this.otherPlayers = [];
  this.boxes = [];
  this.walls = [];
  this.empties = [];
  this.bombs = [];
  this.fires = [];
  this.boosts = [];

  
  this.animationFrameId = 0;
  this.gameBackground = Drawing.createImage("/public/js/game/images/game_background.png", 0, 0);
  this.tileImages = Drawing.createImage("/public/js/game/images/bomb_party_v4.png", 0, 0);
  this.boxImage = Drawing.createImage("/public/js/game/images/box.png", 0, 0);
  this.wallImage = Drawing.createImage("/public/js/game/images/wall.png", 0, 0);
  this.stoneImage = Drawing.createImage("/public/js/game/images/stone.png", 0, 0);
  this.bombBoostImage = Drawing.createImage("/public/js/game/images/light.png", 0, 0);
  this.healthBoostImage = Drawing.createImage("/public/js/game/images/health.png", 0, 0);
  this.putabilityBoostImage = Drawing.createImage("public/js/game/images/bomb.png");
  
  
  this.selfImages = [];
  this.othersImages = [];
  this.tileW = 50;
  this.tileH = 50;

  this.createOthersImages("enemy");
}

Game.prototype.updateSocket = function(newSocket){
  this.socket = newSocket;
}

Game.prototype.createSelfImages = function(whois) {
  this.selfImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/l1.png", 0, 0));
  this.selfImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/l2.png", 0, 0));
  this.selfImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/l3.png", 0, 0));
  this.selfImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/r1.png", 0, 0));
  this.selfImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/r2.png", 0, 0));
  this.selfImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/r3.png", 0, 0));
}

Game.prototype.createOthersImages = function(whois) {
  this.othersImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/l1.png", 0, 0));
  this.othersImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/l2.png", 0, 0));
  this.othersImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/l3.png", 0, 0));
  this.othersImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/r1.png", 0, 0));
  this.othersImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/r2.png", 0, 0));
  this.othersImages.push(Drawing.createImage("/public/js/game/images/" + whois + "/r3.png", 0, 0));
}

/**
 * Factory method to create a Game object.
 * @param {Object} socket The Socket connected to the server.
 * @param {Element} canvasElement The canvas element that the game will use to
 * @param {Element} canvasInfoElement The canvas element that the game will use to
 *   draw to.
 * @return {Game}
 */
Game.create = function(socket, canvasElement, canvasInfoElement) {
  /**
   * Set the aspect ratio of the canvas.
   */

  canvasElement.width = 750;
  canvasElement.height = 700;
  // canvasElement.style.border = '1px solid black';

  var canvasContext = canvasElement.getContext('2d');

  // canvasInfoElement.width = 140;
  // canvasInfoElement.height = 50;
  // canvasInfoElement.style.border = '1px solid black';

  var canvasInfoContext = canvasInfoElement.getContext('2d');

  var drawing = Drawing.create(canvasContext, canvasInfoContext);
  return new Game(socket, drawing);
};

/**
 * Initializes the Game object and its child objects as well as setting the
 * event handlers.
 */
Game.prototype.init = function(playerAvatar) {
  var context = this;
  this.socket.on('update', function(data) {
    context.receiveGameState(data);
  });
  this.socket.emit('player-join', playerAvatar);
  
  this.selfImages = [];
  switch(playerAvatar) {
      case 1 : this.createSelfImages('monkey'); break;
      case 2 : this.createSelfImages('gremix'); break;
      case 3 : this.createSelfImages('girl'); break;
      default : this.createSelfImages('gremix'); break;
  }
};


/**
 * This method begins the animation loop for the game.
 */
Game.prototype.animate = function() {
  this.animationFrameId = window.requestAnimationFrame(
      Util.bind(this, this.update));
};

/**
 * This method stops the animation loop for the game.
 */
Game.prototype.stopAnimation = function() {
  window.cancelAnimationFrame(this.animationFrameId);
};

/**
 * Updates the game's internal storage of all the powerups, called each time
 * the server sends packets.
 * @param {Object} state The game state received from the server.
 */
Game.prototype.receiveGameState = function(state) {
  this.selfPlayer = state['self'];
  this.otherPlayers = state['players'];
  this.boxes = state['boxes'];
  this.walls = state['walls'];
  this.empties = state['empties'];
  this.bombs = state['bombs'];
  this.fires = state['fires'];
  this.boosts = state['boosts'];
  console.log("players count :: ", this.otherPlayers.length + 1);
  console.log("boxes :: ", this.boxes.length);
  console.log("walls :: ", this.walls.length);
  console.log("empties :: ", this.empties.length);
  console.log("bombs :: ", this.bombs.length);
  console.log("fires :: ", this.fires.length);
  console.log("boosts :: ", this.boosts.length);
  console.log("*****************************************");
};

/**
 * Updates the state of the game client side and relays intents to the
 * server.
 */
Game.prototype.update = function() {
  if (this.selfPlayer) {
    // Emits an event for the containing the player's input.
    if (Input.SPACE == true) {
      console.log('input.Space :: ', Input.SPACE, ' socket.id : ', this.socket.id);
    }
    // this.socket.id();
    this.socket.emit('player-action', {
      keyboardState: {
        left: Input.LEFT,
        right: Input.RIGHT,
        up: Input.UP,
        down: Input.DOWN,
        space: Input.SPACE
      }
    });
    this.draw();
    if (Input.SPACE == true) {
      Input.SPACE = false;
    }
  }
  this.animate();
};

/**
 * Draws the state of the game using the internal Drawing object.
 */
Game.prototype.draw = function() {
  // Clear the canvas.
  this.drawing.clear();

  this.drawing.drawBackground(this.gameBackground);
  // draw yourself
  //tileImages, 16*1, 16*4, 16, 16, x * tileW, y * tileH, tileW, tileH
  // this.drawing.showEmpties(this.tileImages, this.empties, this.tileW, this.tileH);

  this.drawing.showWalls(this.wallImage, this.stoneImage, this.walls, this.tileW, this.tileH);

  this.drawing.showBoxes(this.boxImage, this.boxes, this.tileW, this.tileH);

  this.drawing.showBombs(this.tileImages, this.bombs, this.tileW, this.tileH);

  this.drawing.showFires(this.tileImages, this.fires, this.tileW, this.tileH);

  for (var boost of this.boosts) {
    switch(boost.type) {
      case "bombBoost" : this.drawing.showBoost(this.bombBoostImage, boost, this.tileW, this.tileH); break;
      case "healthBoost" : this.drawing.showBoost(this.healthBoostImage, boost, this.tileW, this.tileH); break;
      case "bombPutabilityBoost" : this.drawing.showBoost(this.putabilityBoostImage, boost, this.tileW, this.tileH); break;
      default : break;
    }
  }

  if (this.selfPlayer.active == true) {
    var tmp_id = parseInt(this.selfPlayer.frameId);
    if (tmp_id >= this.selfPlayer.frameLimit) {
      tmp_id -= 1;
    }
    
    this.drawing.showSelf(this.selfImages[tmp_id], this.selfPlayer, this.tileW, this.tileH);

    this.drawing.showPlayerLife(this.selfPlayer);
  }

  // draw the other players
  for (var player of this.otherPlayers) {
    if (player.active == true) {
      var tmp_id = parseInt(player.frameId);
      if (tmp_id >= player.frameLimit) {
        tmp_id -= 1;
      }
      this.drawing.showOther(this.othersImages[tmp_id], player, this.tileW, this.tileH);
  }
  }

};
