// import { start } from "repl";

const playBtn = document.querySelector(".play-button");
const closeBtn = document.querySelector(".close-button");
const backBtn = document.querySelector(".main-menu-button");
const charOneBtn = document.querySelector(".char-button-1");
const charTwoBtn = document.querySelector(".char-button-2");
const charThreeBtn = document.querySelector(".char-button-3");
const checkBtn = document.querySelector(".check-button");
const menu = document.querySelector(".menu");
const selection = document.querySelector(".selection");
const gameScreen = document.querySelector(".gameScreen");
var player = -1;
var docReady = false;
var game = null;
var socket = null;
var canvas = null;
var infoCanvas = null;

$(document).ready(function() {
  console.log("ready");
  docReady = true;
  canvas = document.getElementById('canvas');
  infoCanvas = document.getElementById('playerLife')
  Input.applyEventHandlers();
  Input.addMouseTracker(canvas);
});


function start_game() {
  if (socket == null) {
    socket = io();
  }  else {
      socket.connect();
  }
  console.log('begin : ', socket.id);
  
  if (game == null) {
      
    console.log(infoCanvas.getContext('2d'));

    game = Game.create(socket, canvas, infoCanvas);
  } else {
      game.updateSocket(socket);
  }
  
  game.init(player);
  game.animate();
}


// Play Button
playBtn.addEventListener('click', function() {
    menu.style.display = 'none';
    selection.style.display = 'inline';
});

// Close Button
closeBtn.addEventListener('click', function() {
    menu.style.display = 'inline';
    selection.style.display = 'none';
    player = -1;
    // todo close socket;
});

// Choose Character
charOneBtn.addEventListener('click', function() {
    player = 1;
    document.querySelector(".playerAvatar").style.backgroundImage = "url('/static/img/gamescreen/avatar2right.png')";
});

charTwoBtn.addEventListener('click', function() {
    player = 2;
    document.querySelector(".playerAvatar").style.backgroundImage = "url('/static/img/gamescreen/avatar1right.png')";
});

charThreeBtn.addEventListener('click', function() {
    player = 3;
    document.querySelector(".playerAvatar").style.backgroundImage = "url('/static/img/gamescreen/avatar6right.png')";
});

// Check & Play
checkBtn.addEventListener('click', function() {
    if(player !== -1 && docReady == true) {
        selection.style.display = 'none';
        gameScreen.style.display = 'inline';
        console.log('play button pressed');
        if (docReady) {
            start_game();
        }
    }
});

backBtn.addEventListener('click', function() {
    selection.style.display = 'none';
    gameScreen.style.display = 'none';
    menu.style.display = 'inline';
    player = -1;
    game.stopAnimation();
    socket.disconnect();
});


