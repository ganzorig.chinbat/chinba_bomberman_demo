
const Util = require('../shared/Util');

function Entity2D(position, velocity, acceleration, orientation, mass, hitbox) {
  this.position = position || [0, 0];
  this.velocity = velocity || [0, 0];
  this.acceleration = acceleration || [0, 0];
  this.orientation = orientation || 0;
  this.mass = mass || 1;
  this.hitbox = hitbox || 0;

  Util.splitProperties(this, ['x', 'y'], 'position');
  Util.splitProperties(this, ['vx', 'vy'], 'velocity');
  Util.splitProperties(this, ['ax', 'ay'], 'acceleration');
}


module.exports = Entity2D;
