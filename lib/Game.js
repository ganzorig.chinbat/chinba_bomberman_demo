const HashMap = require('hashmap');
const Player = require('./Player');

const Wall = require('./Wall');
const Empty = require('./Empty');
const Box = require('./Box');
const Bomb = require('./Bomb');
const Fire = require('./Fire');
const Boost = require('./Boost');

const Util = require('../shared/Util');

const WALL = 1;
const EMPTY = 0;
const BOX = 2;
const BOMB = 3;
const FIRE = 4;

/**
 * Constructor for a Game object.
 * @constructor
 */
function Game() {
  
  var gameMap = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 1, 0, 0, 2, 2, 2, 2, 2, 0, 0, 1, 0],
    [0, 1, 0, 1, 2, 1, 2, 1, 2, 1, 0, 1, 0],
    [0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0],
    [0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 0],
    [0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0],
    [0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 0],
    [0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0],
    [0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 0],
    [0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0],
    [0, 1, 0, 1, 2, 1, 2, 1, 2, 1, 0, 1, 0],
    [0, 1, 0, 0, 2, 2, 2, 2, 2, 0, 0, 1, 0],
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]       
  ];

  this.clients = new HashMap();
  this.players = new HashMap();
  this.boxes = [];
  this.walls = [];
  this.empties = [];
  this.bombs = [];
  this.fires = [];
  this.boosts = [];

  var zero  = 0;
  var one = 0;
  var two = 0;
  var mapW = 15, mapH = 13;
  for (var y = 0; y < mapH; y ++ ) {
    for (var x = 0; x < mapW; x ++) {
        // this.addEmpty(x, y);  
        switch (gameMap[x][y]) {
            case 1: this.addWall(x, y);
            break;
            case 2:
              var res = parseInt(Math.random() * 100) % 7;
              // console.log(res);
              switch (res) {
                case 0: 
                  this.addBox(x, y, "bombBoost");  
                  zero += 1;
                  break;
                case 1: this.addBox(x, y, "healthBoost"); 
                  one += 1;
                  break;
                case 2 : this.addBox(x, y, "bombPutabilityBoost"); 
                  two += 1;
                break;
                default : this.addBox(x, y, ""); break;
              }
            break;
        }
    }
  }
}

/**
 * Factory method for a Game object.
 * @return {Game}
 */
Game.create = function() {
  return new Game();
};

/**
 * Returns a list containing the connected Player objects.
 * @return {Array<Player>}
 */
Game.prototype.getPlayers = function() {
  return this.players.values();
};

/**
 * Returns callbacks that can be passed into an update()
 * method for an object so that it can access other elements and
 * entities in the game.
 * @return {Object<string, Function>}
 */
Game.prototype._callbacks = function() {
  return {
    players: Util.bind(this, this.players)
  };
};

Game.prototype.addNewPlayer = function(socket, playerAvatar) {
  this.clients.set(socket.id, socket);
  var initx = [2, 2, 12, 12];
  var inity = [2, 10, 2, 10];
  var posxy = this.getPlayers().length;
  console.log("players count : ", posxy);
  if (posxy >= 4) return ; 
  posxy %= 4;
  this.players.set(socket.id, Player.create(socket.id, initx[posxy], inity[posxy], playerAvatar));
};

Game.prototype.removePlayer = function(id) {
  this.clients.remove(id);
  this.players.remove(id);
}

Game.prototype.getPlayerCount = function() {
  var len = this.getActivePlayers().length;
  return len;
}

Game.prototype.addBox = function(x, y, boost) {
  this.boxes.push(Box.create(x, y, boost));
};

Game.prototype.addWall = function(x, y) {
  this.walls.push(Wall.create(x, y));
};

Game.prototype.addEmpty = function(x, y) {
  this.empties.push(Empty.create(x, y));
};

Game.prototype.addBoost = function(x, y, boostType) {
  this.boosts.push(Boost.create(x, y, boostType))
}

Game.prototype.addBomb = function(x, y, id, bombExplosionLimit) {
  x = parseInt(x + 0.5);
  y = parseInt(y + 0.5);
  var exists = false;
  for (bomb of this.bombs) {
    if (bomb.active == false) continue; 
    exists |= Util.isCollidedWith3([x, y], bomb) 
  }
  if (exists == false) {
    this.bombs.push(Bomb.create(x, y, id, bombExplosionLimit));
    return true;
  }
  return false;
};

Game.prototype.addFire = function(x, y) {
  x = parseInt(x + 0.5);
  y = parseInt(y + 0.5);
  
  this.fires.push(Fire.create(x, y));
}


/**
 * Updates a player based on input received from their client.
 * @param {string} id The socket ID of the client
 * @param {Object} data The input received from the client
 */
Game.prototype.updatePlayerOnInput = function(id, data) {
  // console.log('id :: ', id);
  var player = this.players.get(id);
  // console.log(player.avatar);
  if (player) {
      if (data.keyboardState.space) {
        if (player.active == true && player.bombPutability > 0) {
          console.log('player.bomblimit : ', player.bombExplosionLimit);
          var ok = this.addBomb(player.x, player.y, player.id, player.bombExplosionLimit);
          if (ok) {
            player.bombPutability -= 1; 
          }
        }
      } 
      player.updateOnInput(data.keyboardState);
  }
}


Game.prototype.clearInActiveEntities = function(){
  
  var tmp_bombs = [];
  for (var i = 0; i < this.bombs.length; i ++ ) {
    if (this.bombs[i].active == true) {
      tmp_bombs.push(this.bombs[i]);
    } else {
      delete this.bombs[i];
    }
  }
  this.bombs = tmp_bombs;
  
  var tmp_fires = [];
  for (var i = 0; i < this.fires.length; i ++ ) {
    if (this.fires[i].active == true) {
      tmp_fires.push(this.fires[i]);
    } else {
      delete this.fires[i];
    }
  }
  this.fires = tmp_fires;


  var tmp_boxes = [];
  for (var i = 0; i < this.boxes.length; i ++ ) {
    if (this.boxes[i].inFire == false) {
      tmp_boxes.push(this.boxes[i]);
    } else {
      if(this.boxes[i].boostType != "") {
        console.log("boost.type :: ", this.boxes[i].boostType);
        this.addBoost(this.boxes[i].x, this.boxes[i].y, this.boxes[i].boostType);
      }
      delete this.boxes[i];
    }
  }
  this.boxes = tmp_boxes;

  var tmp_boosts = [];
  for (var i = 0; i < this.boosts.length; i ++) {
    if (this.boosts[i].active == true) {
      tmp_boosts.push(this.boosts[i]);
    } else {
      delete this.boosts[i];
    }
  }
  this.boosts = tmp_boosts;

}

Game.prototype.getActivePlayers = function() {
  var tmp_players = [];
  var players = this.getPlayers();
  for (var i = 0; i < players.length; i ++) {
      if (players[i].inFire == true ) {
        if (players[i].lifeNo  <= 0) {
          players[i].active = false;
        } else {
          players[i].lifeNo -= 1;
          players[i].inFire = false;
        }
      }
      if (players[i].active == false) {
        players[i].active = false;
        // this.removePlayer(players[i].id);
      } else {
        tmp_players.push(players[i]);
      }
  }
  players = tmp_players;

  return players;
}


/**
 * Steps the server forward in time. Updates every entity in the game.
 */
Game.prototype.update = function() {
  
  this.clearInActiveEntities();

  var players = this.getActivePlayers();

  // var players = this.getPlayers();
  for (var i = 0; i < players.length; ++i) {
    players[i].update(this.walls, this.boxes, this.bombs);
  }
  

  for (var i = 0; i < this.fires.length; i ++) {
    this.fires[i].updateFrame(players, this.boxes, this.bombs);
  }

  for (var i = 0; i < this.bombs.length; i ++) {
    this.bombs[i].updateFrame();
    if (this.bombs[i].active == false || this.bombs[i].inFire == true) {
        this.bombs[i].active = false;
        this.addFire(this.bombs[i].x, this.bombs[i].y);
        var player = this.players.get(this.bombs[i].player_id);
        player.bombPutability += 1;
        this.spreadFires(this.bombs[i].x, this.bombs[i].y, this.bombs[i].level, 0, 0);
    }
  }
  
  for (var i = 0; i < this.boosts.length; i ++) {
    if (this.boosts[i].active == true) {
      this.boosts[i].checkCollapseWithPlayer(players);
    }
  }

};

var dx = [1, -1, 0, 0];
var dy = [0, 0, -1, 1];


Game.prototype.collapsedWithWall = function (x, y) {
  var collapsed = false;
  for (var wall of this.walls) {
    var res = Util.isCollidedWith3([x, y], wall);
    if (res == true) {
      collapsed = true;
      break;
    }
  }
  return collapsed;
}


Game.prototype.collapsedWithBox = function (x, y) {
  var collapsed = false;
  for (var box of this.boxes) {
    var res = Util.isCollidedWith3([x, y], box);
    if (res == true) {
      collapsed = true;
      break;
    }
  }
  return collapsed;
}


Game.prototype.collapsedWithBomb = function (x, y) {
  var collapsed = false;
  for (var bomb of this.bombs) {
    var res = Util.isCollidedWith3([x, y], bomb);
    if (res == true) {
      bomb.inFire = true;
      collapsed = true;
      break;
    }
  }
  return collapsed;
}


/**
 * Bomb explosion fire spreads 4 directions.
 */
Game.prototype.spreadFires = function(x, y, level, todx, tody) {
    if (level == 0) return ;
    if (todx == 0 && tody == 0) {
    for (var i = 0; i < 4; i ++) {
        var tmpx = x + dx[i];
        var tmpy = y + dy[i];
        if (tmpx < 0 || tmpx >= this.mapW || tmpy < 0 || tmpy >= this.mapH) {
          continue;
        }
        if (this.collapsedWithWall(tmpx, tmpy) == true)  {
          continue;
        }
        if (this.collapsedWithBox(tmpx, tmpy) == true) {
          this.addFire(tmpx, tmpy);
          continue;
        }
        if (this.collapsedWithBomb(tmpx, tmpy) == true) {
          this.addFire(tmpx, tmpy);
          continue;
        }
        this.addFire(tmpx, tmpy);
        this.spreadFires(tmpx, tmpy, level - 1, dx[i], dy[i]);
    }
  } else {
        var tmpx = x + todx;
        var tmpy = y + tody;
        if (tmpx < 0 || tmpx >= this.mapW || tmpy < 0 || tmpy >= this.mapH) {
          return ;
        }   
        if (this.collapsedWithWall(tmpx, tmpy) == true)  {
          return ;
        }
        if (this.collapsedWithBox(tmpx, tmpy) == true) {
          this.addFire(tmpx, tmpy);
          return ;
        }
        if (this.collapsedWithBomb(tmpx, tmpy) == true) {
          this.addFire(tmpx, tmpy);
          return ;
        }
        this.addFire(tmpx, tmpy);
        this.spreadFires(tmpx, tmpy, level - 1, todx, tody);
  }
}


/**
 * Sends the state of the game to every client.
 */
Game.prototype.sendState = function() {
  var ids = this.clients.keys();
  for (var i = 0; i < ids.length; ++i) {
    this.clients.get(ids[i]).emit('update', {
      self: this.players.get(ids[i]),
      players: this.players.values().filter((player) => player.id != ids[i]),
      boxes: this.boxes,
      empties: this.empties,
      walls: this.walls,
      bombs: this.bombs,
      fires: this.fires,
      boosts: this.boosts
    });
  }
};

module.exports = Game;
