/**
 * @fileoverview This is a class encapsulating a Wall.
 * @author chinbat ganzorig
 */

const Entity2D = require('./Entity2D');

const Util = require('../shared/Util');

/**
 * Constructor for a Wall
 * @constructor
 * @param {Int} x The position x 
 * @param {Int} y The position y
 */
function Wall(x, y) {
  Entity2D.call(this, [x, y], null, null, null, null, Wall.HITBOX);
}

Util.extend(Wall, Entity2D);

Wall.HITBOX = 0.45;

/**
 * Factory method for creating a Wall
 * @param {Int} x The position x 
 * @param {Int} y The position y
 * @return {Wall}
 */
Wall.create = function(x, y) {
  return new Wall(x, y);
};


module.exports = Wall;
