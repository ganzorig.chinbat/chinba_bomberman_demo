/**
 * @fileoverview This is a class encapsulating a Box.
 * @author chinbat ganzorig
 */

const Entity2D = require('./Entity2D');

const Util = require('../shared/Util');

/**
 * Constructor for a Box
 * @constructor
 * @param {Int} x The position x 
 * @param {Int} y The position y
 */
function Box(x, y, boostType) {
  Entity2D.call(this, [x, y], null, null, null, null, Box.HITBOX);
  this.inFire = false;
  this.boostType = boostType;
}

Util.extend(Box, Entity2D);

Box.HITBOX = 0.4;

/**
 * Factory method for creating a Box
 * @param {Int} x The position x 
 * @param {Int} y The position y
 * @return {Box}
 */
Box.create = function(x, y, boostType) {
  return new Box(x, y, boostType);
};


module.exports = Box;
