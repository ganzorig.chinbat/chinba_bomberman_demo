/**
 * @fileoverview This is a class encapsulating a Boost.
 * @author chinbat ganzorig
 */

const Entity2D = require('./Entity2D');

const Util = require('../shared/Util');

/**
 * Constructor for a Boost
 * @constructor
 * @param {Int} x The position x 
 * @param {Int} y The position y
 */
function Boost(x, y, type) {
  Entity2D.call(this, [x, y], null, null, null, null, Boost.HITBOX);
  // bombExplosionLevelUp;
  // playerLifeUp;
  // playerSpeedUp;
  // playerBombNoUP;
  this.type = type;
  this.active = true;
}

Util.extend(Boost, Entity2D);

Boost.HITBOX = 1;

/**
 * Factory method for creating a Boost
 * @param {Int} x The position x 
 * @param {Int} y The position y
 * @return {Empty}
 */
Boost.create = function(x, y, type) {
  return new Boost(x, y, type);
};


Boost.prototype.checkCollapseWithPlayer = function(players) {
  for (var player of players) {
      if (player.active == false) continue;
      if (this.active == false) return ;
      var res = Util.isCollidedWith3([this.x, this.y], player);
      if (res == true) {
        switch(this.type) {
          case "bombBoost" : player.bombExplosionLimit += 1; break;
          case "healthBoost" : player.lifeNo += 20; break;
          case "bombPutabilityBoost" : player.bombPutability += 1; break;
          default : break;
        }
        this.active = false;
        return ;
      }
  }
}


module.exports = Boost;
