/**
 * @fileoverview This is a class encapsulating a Fire.
 * @author chinbat ganzorig
 */

const Entity2D = require('./Entity2D');

const Util = require('../shared/Util');

/**
 * Constructor for a Bomb
 * @constructor
 * @param {Int} x The position x 
 * @param {Int} y The position y
 */
function Fire(x, y) {
  Entity2D.call(this, [x, y], null, null, null, null, Fire.HITBOX);
  this.create_time;
  this.lastUpdateTime = 0;
  this.imageW = [14, 14, 14, 14, 14];
  this.imageH = [ 3,  4,  5,  4,  3];
  this.active = true;
  this.frameId = 0;
  this.frameLimit = 5;
  this.inFireFrameLimit = 3;
}

Util.extend(Fire, Entity2D);

Fire.HITBOX = 0.45;

/**
 * Factory method for creating a Fire
 * @param {Int} x The position x 
 * @param {Int} y The position y
 * @return {Fire}
 */
Fire.create = function(x, y) {
  return new Fire(x, y);
};


Fire.prototype.updateFrame = function(players, boxes, bombs) {
    var currentTime = (new Date()).getTime();
    
    if (parseInt(this.frameId) <= this.inFireFrameLimit) {
    
      for (player of players) {
        var res = Util.isCollidedWith4([this.x, this.y], player);
        if (res == true) {
          player.inFire = true;
        }
      }

      for (box of boxes) {
        var res = Util.isCollidedWith3([this.x, this.y], box);
        if (res == true) {
          box.inFire = true;
        }
      }

      for (bomb of bombs) {
        var res = Util.isCollidedWith3(this.x, this.y, bomb);
        if (res == true) {
          bomb.inFire = true;
        }
      }
    }
    this.frameId += 0.16;
    
    if (parseInt(this.frameId) == this.frameLimit) {
        this.active = false;
    } 

    this.lastUpdateTime = currentTime;
};



module.exports = Fire;
