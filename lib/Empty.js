/**
 * @fileoverview This is a class encapsulating a Empty.
 * @author chinbat ganzorig
 */

const Entity2D = require('./Entity2D');

const Util = require('../shared/Util');

/**
 * Constructor for a Empty
 * @constructor
 * @param {Int} x The position x 
 * @param {Int} y The position y
 */
function Empty(x, y) {
  Entity2D.call(this, [x, y], null, null, null, null, Empty.HITBOX);
}

Util.extend(Empty, Entity2D);

Empty.HITBOX = 1;

/**
 * Factory method for creating a Empty
 * @param {Int} x The position x 
 * @param {Int} y The position y
 * @return {Empty}
 */
Empty.create = function(x, y) {
  return new Empty(x, y);
};


module.exports = Empty;
