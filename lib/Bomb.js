/**
 * @fileoverview This is a class encapsulating a Bomb.
 * @author chinbat ganzorig
 */

const Entity2D = require('./Entity2D');

const Util = require('../shared/Util');

/**
 * Constructor for a Bomb
 * @constructor
 * @param {Int} x The position x 
 * @param {Int} y The position y
 */
function Bomb(x, y, id, bombExplosionLimit) {
  Entity2D.call(this, [x, y], null, null, null, null, Bomb.HITBOX);
  this.create_time;
  this.lastUpdateTime = 0;
  this.imageW = [11, 11, 11];
  this.imageH = [2, 3, 4 ];
  this.active = true;
  this.frameId = 0;
  this.frameLimit = 3;
  this.inFire = false;
  this.player_id = id;
  this.player_out = false;
  this.level = bombExplosionLimit;
  
}

Util.extend(Bomb, Entity2D);

Bomb.HITBOX = 0.45;

/**
 * Factory method for creating a Wall
 * @param {Int} x The position x 
 * @param {Int} y The position y
 * @return {Bomb}
 */
Bomb.create = function(x, y, id, bombExplosionLimit) {
  console.log('factory method create : ', bombExplosionLimit);
  return new Bomb(x, y, id, bombExplosionLimit);
};


Bomb.prototype.updateFrame = function() {
    var currentTime = (new Date()).getTime();
    
    this.frameId += 0.035;
    
    if (parseInt(this.frameId) == this.frameLimit) {
        this.active = false;
    }
    this.lastUpdateTime = currentTime;
    };

module.exports = Bomb;
