const Entity2D = require('./Entity2D');

const Util = require('../shared/Util');

/**
 * Constructor for a Player
 * @constructor
 * @param {string} id The socket ID of the Player
 */
function Player(id, x, y, playerAvatar) {
  Entity2D.call(this, [x, y], null, null, null, null, Player.HITBOX);
  this.id = id;
  this.lastUpdateTime = 0;
  this.deltaTime = 0;
  this.lifeNo = 100;
  this.isInfire = false;
  this.active = true;

  this.bombPutability = 1;
  this.bombExplosionLimit = 1;

  this.movement_dirs = ["none"];
  
  this.frameId = 0;

  this.frameRightBeg = 3;
  this.frameRightFin = 6;

  this.frameLeftBeg = 0;
  this.frameLeftFin = 3;
  this.frameLimit = 6;
  switch(playerAvatar) {
    case 1 : this.avatar = "monkey"; break;
    case 2 : this.avatar = "gremix"; break;
    case 3 : this.avatar = "girl"; break;
    default : this.avatar = "gremix";
    break;
  }
}
Util.extend(Player, Entity2D);

Player.HITBOX = 0.45;

/**
 * Factory method for creating a Player
 * @param {string} id The socket ID of the Player
 * @return {Player}
 */
Player.create = function(id, x, y, playerAvatar) {
  return new Player(id, x, y, playerAvatar);
};

/**
 * Updates the Player based on received input.
 * @param {Object} keyboardState The keyboard input received.
 */
Player.prototype.updateOnInput = function(keyboardState) {
  var speed = 2.5;
  // console.log('keyboard State : ', keyboardState);
  this.vy = speed * (Number(keyboardState.down) - Number(keyboardState.up));
  this.vx = speed * (Number(keyboardState.right) - Number(keyboardState.left));

};


Player.prototype.isCollidedWithSth = function(walls, boxes, bombs) {
  var tmp_position = [this.position[0], this.position[1]];
  var tmp_velocity = [this.velocity[0], this.velocity[1]];
  var tmp_acceleration = [this.acceleration[0], this.acceleration[1]];

  for (var i = 0; i < 2; ++i) {
    tmp_position[i] += this.velocity[i] * this.deltaTime;
    tmp_velocity[i] += this.acceleration[i] * this.deltaTime;
    tmp_acceleration[i] = 0;
  }
  var isCollided = false;
  for (var i = 0; i < walls.length; i ++) {
    var tmp_res = Util.isCollidedWith3(tmp_position, walls[i]);
    if (tmp_res) {
      isCollided = true;
      return true;
    }
  }
  for (var i = 0; i < boxes.length; i ++) {
    var tmp_res = Util.isCollidedWith3(tmp_position, boxes[i]);
    if (tmp_res) {
      isCollided = true;
      return true;
    }
  }
  for (var i = 0; i < bombs.length; i ++) {
    if (bombs[i].active == false) continue;
    var tmp_res = Util.isCollidedWith3(tmp_position, bombs[i]);
        
    if (bombs[i].player_id == this.id) {
        if (bombs[i].player_out == false ) {
          if (Util.isCollidedWith3(tmp_position, bombs[i]) == false) {
              bombs[i].player_out = true;
          } else {
            tmp_res = false;
          }
        } 
    }

    if (tmp_res) {
      isCollided = true;
      return true;
    }

  }
  return isCollided;
}


Player.prototype.isCollidedWithWho = function(walls, boxes, bombs) {
  var tmp_position = [this.position[0], this.position[1]];
  var tmp_velocity = [this.velocity[0], this.velocity[1]];
  var tmp_acceleration = [this.acceleration[0], this.acceleration[1]];

  for (var i = 0; i < 2; ++i) {
    tmp_position[i] += this.velocity[i] * this.deltaTime;
    tmp_velocity[i] += this.acceleration[i] * this.deltaTime;
    tmp_acceleration[i] = 0;
  }
  var isCollided = false;
  for (var i = 0; i < walls.length; i ++) {
    var tmp_res = Util.isCollidedWith3(tmp_position, walls[i]);
    if (tmp_res) {
      isCollided = true;
      return walls[i];
    }
  }
  for (var i = 0; i < boxes.length; i ++) {
    var tmp_res = Util.isCollidedWith3(tmp_position, boxes[i]);
    if (tmp_res) {
      isCollided = true;
      return boxes[i];
    }
  }
  for (var i = 0; i < bombs.length; i ++) {
    if (bombs[i].active == false) continue;
    var tmp_res = Util.isCollidedWith3(tmp_position, bombs[i]);
        
    if (bombs[i].player_id == this.id) {
        if (bombs[i].player_out == false ) {
          if (Util.isCollidedWith3(tmp_position, bombs[i]) == false) {
              bombs[i].player_out = true;
          } else {
            tmp_res = false;
          }
        } 
    }

    if (tmp_res) {
      isCollided = true;
      return bombs[i];
    }

  }
  return null;
}



Player.prototype.getDirection = function(vsx, vsy) {
  
  if (vsx == 0 && vsy > 0) {
    return "down";
  } else 
  if (vsx == 0 && vsy < 0) {
    return "up";
  } else 
  if (vsx > 0 && vsy == 0) {
      return "right";
  } else
  if (vsx < 0 && vsy == 0) {
      return "left";
  }
  return "none";
}

Player.prototype.setLimit = function() {
  if (this.movement_dirs.length > 5) {
    this.movement_dirs.reverse();
    this.movement_dirs.pop();
    this.movement_dirs.reverse();
  }
}


Player.prototype.update = function(walls, boxes, bombs, deltaTime) {

  this.setLimit();
  var currentTime = (new Date()).getTime();
  if (deltaTime) {
    this.deltaTime = deltaTime;
  } else if (this.lastUpdateTime === 0) {
    this.deltaTime = 0;
  } else {
    this.deltaTime = (currentTime - this.lastUpdateTime) / 1000;
  }
  var enty = this.isCollidedWithWho(walls, boxes, bombs);

  if (enty != null) {

    if (this.velocity[0] == 0 && this.velocity[1] > 0) {
        // go down;
        var dif = Util.isCollidedWithDown([this.x, this.y], enty);
        if (dif != 0) {

          if (dif < 0) {
            this.velocity[0] = this.velocity[1]; 
            this.velocity[1] = 0;
          }
          if (dif > 0) {
            this.velocity[0] = -this.velocity[1]; 
            this.velocity[1] = 0;  
          } 
        }
    } else 
    if (this.velocity[0] == 0 && this.velocity[1] < 0) {
        // go up;
        var dif = Util.isCollidedWithUp([this.x, this.y], enty);
        if (dif != 0) {
          if (dif > 0) {
            this.velocity[0] = this.velocity[1]; 
            this.velocity[1] = 0;
          } 
          if (dif < 0) {
            this.velocity[0] = -this.velocity[1];
            this.velocity[1] = 0;
          }
        }
    } else 
    if (this.velocity[0] > 0 && this.velocity[1] == 0) {
        // go right;
        var dif = Util.isCollidedWithRight([this.x, this.y], enty);

        if (dif < 0) {  
          this.velocity[1] = this.velocity[0]; 
          this.velocity[0] = 0;
        } 
        if (dif > 0) {
          this.velocity[1] = -this.velocity[0]; 
          this.velocity[0] = 0;  
        }
    } else
    if (this.velocity[0] < 0 && this.velocity[1] == 0) {
        // go left;
        
        var dif = Util.isCollidedWithLeft([this.x, this.y], enty);
          
        if (dif > 0) {  
          this.velocity[1] = this.velocity[0]; 
          this.velocity[0] = 0;
        } 
        if (dif < 0) {
          this.velocity[1] = -this.velocity[0]; 
          this.velocity[0] = 0;  
        }
    }

    if (this.isCollidedWithSth(walls, boxes, bombs) == false) { 
      var dir = this.getDirection(this.velocity[0], this.velocity[1]);
      var len = this.movement_dirs.length;
      var last = this.movement_dirs[len - 1];
      if (dir != "none" && last != dir) {
        console.log(last, dir);
        this.movement_dirs.push(dir);
      }
      this.moveAnimation(last, dir);
      for (var i = 0; i < 2; ++i) {
        this.position[i] += this.velocity[i] * this.deltaTime;
        this.velocity[i] += this.acceleration[i] * this.deltaTime;
        this.acceleration[i] = 0;
      }
   }
    this.lastUpdateTime = currentTime;
    return ;
  }
  var dir = this.getDirection(this.velocity[0], this.velocity[1]);
  
  var len = this.movement_dirs.length;
  var last = this.movement_dirs[len - 1];
  if (dir != "none" && last != dir) {
    // console.log(last, dir);
    this.movement_dirs.push(dir);
  }
  
  this.moveAnimation(last, dir);

  for (var i = 0; i < 2; ++i) {
    this.position[i] += this.velocity[i] * this.deltaTime;
    this.velocity[i] += this.acceleration[i] * this.deltaTime;
    this.acceleration[i] = 0;
  }

  this.lastUpdateTime = currentTime;
};

Player.prototype.moveAnimation = function (last, dir) {
  if (dir != "none" && last != dir) {
    if (dir == "up") {
      this.frameId = this.frameLeftBeg;
    }
    if (dir == "down") {
      this.frameId = this.frameLeftBeg;
    }
    if (dir == "right") {
      this.frameId = this.frameRightBeg;
    }
    if (dir == "left") {
      this.frameId = this.frameLeftBeg;
    }
  }
  if (dir == "none" && last != "none") {
    if (last == "up") {
      this.frameId = this.frameLeftBeg;
    }
    if (last == "down") {
      this.frameId = this.frameLeftBeg;
    }
    if (last == "right") {
      this.frameId = this.frameRightBeg;
    }
    if (last == "left") {
      this.frameId = this.frameLeftBeg;
    }
  }

  if (dir != "none") {
    this.frameId += 0.16;
    
    if (dir == "up") {
      if (parseInt(this.frameId) == this.frameRightFin) {
        this.frameId = this.frameLeftBeg + 1;
      }
    }

    if (dir == "down") {
      if (parseInt(this.frameId) == this.frameRightFin) {
        this.frameId = this.frameLeftBeg + 1;
      }
    }

    if (dir == "right") {
      if (parseInt(this.frameId) == this.frameRightFin) {
        this.frameId = this.frameRightBeg + 1;
      }
    }

    if (dir == "left") {
      if (parseInt(this.frameId) == this.frameLeftFin) {
        this.frameId = this.frameLeftBeg + 1;
      }
    }
  }

}

module.exports = Player;
