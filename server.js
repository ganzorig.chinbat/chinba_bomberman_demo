const DEV_MODE = process.argv.indexOf('--dev') != -1;
const FPS = 60;
const PORT = process.env.PORT || 5000;

// Dependencies.
const express = require('express');
const http = require('http');
const morgan = require('morgan');
const socketIO = require('socket.io');
var path = require('path');

const Game = require('./lib/Game');

// Initialization.
var app = express();
var server = http.Server(app);
var io = socketIO(server);
var game = null;

app.set('port', PORT);

app.use(morgan('dev'));
app.use('/public', express.static(__dirname + '/public'));
app.use('/shared', express.static(__dirname + '/shared'));
app.use(express.static(__dirname + '/views'));

app.get('/', function(request, response) {
  console.log("hahaha!");
  response.sendFile(path.join(__dirname + '/views', 'index.html'));
});

/**
 * Server side input handler, modifies the state of the players and the
 * game based on the input it receives. Everything here runs asynchronously.
 */
io.on('connection', (socket) => {
  
  socket.on('game-start', () => {
    console.log("hahahaha start message arrived");
  });

  socket.on('player-join', (playerAvatar) => {
    if (game == null) {
      game = Game.create();
    }
    game.addNewPlayer(socket, playerAvatar);
  });

  socket.on('player-action', (data) => {
    if (game != null) {
      game.updatePlayerOnInput(socket.id, data);
    }
  });

  socket.on('disconnect', () => {
    console.log("disconnected");
    if (game != null) {
      game.removePlayer(socket.id);
      if (game.getPlayerCount() < 1) {
          delete game;
          game = null;
      }
    }
  });

});

/**
 * Server side game loop. This runs at 60 frames per second.
 */
setInterval(() => {
  if (game != null) {
    game.update();
    game.sendState();
  }
}, 1000 / FPS);

/**
 * Starts the server.
 */
server.listen(PORT, function() {
  console.log(`STARTING SERVER ON PORT ${PORT}`);
  if (DEV_MODE) {
    console.log('DEVELOPMENT MODE ENABLED')
  }
});
